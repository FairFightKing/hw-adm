FROM node:lts-alpine

RUN yarn global add http-server

WORKDIR /app

COPY package*.json ./
COPY yarn.lock ./

RUN npm install

COPY . .

RUN npm run build

EXPOSE 8080
CMD ["nginx", "-g", "daemon off;"]